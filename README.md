# Nagłówek
paragraf jeden

paragraf dwa

paragraf trzy

**Podgrubienie**, *kursywa* i ~~przekreślenia~~ :))
> "Życie jest za krótkie, aby coś robić dobrze" - Tomek

1. Lista numerowana
   1. Podpunkt 1
   2. Podpunkt 2
      1. Podpunkt 2.1
      2. Podpunkt 2.2

- Lista nienumerowana
  - Podpunkt A
  - Podpunkt B
    - Podpunkt B.1
    - Podpunkt B.2

```py
def hello_world():
    print("Hello, world!")
    print("World")
    print("!!!")
```

for i in range(3):
    hello_world()



![Obraz](zdjęcie.jpg)
